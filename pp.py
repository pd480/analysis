import pandas as pd
import numpy as np
import datetime
from sklearn.cluster import KMeans


import matplotlib.pyplot as plt
import squarify
from io import BytesIO

def da(a):
    
    Filename=a

    
    df= pd.read_csv(Filename,sep='|')

    df.isnull().sum()

    df.isnull().any(axis=1)
    df[df.isnull().any(axis=1)]


    
    #As the null values are less, drop the null rows. 
    df.dropna(inplace=True)
    df.head()


    
    #checking the data type of each column.
    df.info()


    
    #Change the data type of columns.
    df['transaction_date'] = pd.to_datetime(df['transaction_date'], errors='coerce')
    df["cont_id"]=pd.to_numeric(df["cont_id"],downcast ='signed', errors='coerce')
    df["transaction_id"]=pd.to_numeric(df["transaction_id"],downcast ='signed', errors='coerce')
    df["prod_price_net"]=pd.to_numeric(df["prod_price_net"], errors='coerce')
    df["prod_id"]=pd.to_numeric(df["prod_id"],downcast ='signed', errors='coerce')



    #null values check
    df.isnull().sum()


    
    df.isnull().any(axis=1)
    df[df.isnull().any(axis=1)]


    
    #As there is only 1 row with null values in it, drop that row.
    df.dropna(inplace=True)
    df.head()


    
    #as customer id, transaction id, product id can't be float.
    df['cont_id']=df['cont_id'].astype(int)
    df['transaction_id']=df['transaction_id'].astype(int)
    df['prod_id']=df['prod_id'].astype(int)


    
    df.isnull().sum()


    
    df.info()


    
    df.describe()


    
    #identifing earliest and latest dates of transaction. 
    print(df['transaction_date'].min(), df['transaction_date'].max())


    
    #calculating no. days for each purchase
    sd = df['transaction_date'].max() + datetime.timedelta(days=1)
    df['hist']=sd - df['transaction_date']
    df['hist'].astype('timedelta64[D]')
    df['hist']=df['hist'] / np.timedelta64(1, 'D')



    #creating group according to customer_id, and calculating recency, frequency,and monetary values.
    rfmTable = df.groupby('cont_id').agg({'hist': lambda x:x.min(), # Recency
                                            'cont_id': lambda x: len(x), # Frequency
                                            'prod_price_net': lambda x: x.sum()}) # Monetary Value

    rfmTable.rename(columns={'hist': 'recency', 
                            'cont_id': 'frequency', 
                            'prod_price_net': 'monetary_value'}, inplace=True)


    
    rfmTable.shape


    
    rfmTable.head()


    
    #creating 4 categories according to the quartile values.
    quartiles = rfmTable.quantile(q=[0.25,0.50,0.75])
    print(quartiles, type(quartiles))


    
    quartiles=quartiles.to_dict()
    quartiles


    
    #function for recency 1 being the best value.
    def RClass(x,p,d):
        if x <= d[p][0.25]:
            return 1
        elif x <= d[p][0.50]:
            return 2
        elif x <= d[p][0.75]: 
            return 3
        else:
            return 4
        
    ## for Frequency and Monetary value, 1 being the best value. 

    def FMClass(x,p,d):
        if x <= d[p][0.25]:
            return 4
        elif x <= d[p][0.50]:
            return 3
        elif x <= d[p][0.75]: 
            return 2
        else:
            return 1


    
    rfmSeg = rfmTable
    rfmSeg['R_Quartile'] = rfmSeg['recency'].apply(RClass, args=('recency',quartiles,))
    rfmSeg['F_Quartile'] = rfmSeg['frequency'].apply(FMClass, args=('frequency',quartiles,))
    rfmSeg['M_Quartile'] = rfmSeg['monetary_value'].apply(FMClass, args=('monetary_value',quartiles,))


    
    #method1: concatinate the r,f,m scores. As the unique values are more 
    #it is not easy to understand. 
    rfmSeg['RFMClass'] = rfmSeg.R_Quartile.map(str)                             + rfmSeg.F_Quartile.map(str)                             + rfmSeg.M_Quartile.map(str)


    
    rfmSeg.head()


    
    rfmSeg.sort_values(by=['RFMClass', 'monetary_value'], ascending=[True, False])


    
    rfmSeg.groupby('RFMClass').agg('monetary_value').mean()


    
    #Method2: Add the R,F,M scores. As there are only 12 unique values,
    #its easy to understand.
    rfmSeg['Total Score'] = rfmSeg['R_Quartile'] + rfmSeg['F_Quartile'] +rfmSeg['M_Quartile']
    print(rfmSeg.head(), rfmSeg.info())


    
    rfmSeg.groupby('Total Score').agg('monetary_value').mean()


    
    #customers with total score of 3,4,5 adds upto maximum monetary_value.
    fig=rfmSeg.groupby('Total Score').agg('monetary_value').mean().plot(y='Average Monetary',kind='bar', colormap='Blues_r',title="Total_score vs Avg_Monetary").get_figure()
    fig
    fig.savefig('C:/Users/12014/Documents/Python Scripts/PYTHON/Internship/static/fig')


    



    
    #customers with total score of 3,4,5 are more frequent in pruchasing.
    fig1=rfmSeg.groupby('Total Score').agg('frequency').mean().plot(y='Frequency',kind='bar', colormap='Blues_r',title="Total_score vs Avg_Frequency").get_figure()
    fig1
    fig1.savefig('C:/Users/12014/Documents/Python Scripts/PYTHON/Internship/static/fig1')


    
    #the most recent customers are with the total scores of 3,4,5
    fig2=rfmSeg.groupby('Total Score').agg('recency').mean().plot(y='Recency',kind='bar', colormap='Blues_r',title="Total_score vs Avg_Recency").get_figure()
    fig2
    fig2.savefig('C:/Users/12014/Documents/Python Scripts/PYTHON/Internship/static/fig2')


    
    #stimulating the response of customer to promotions.
    col=['R_Quartile','F_Quartile','M_Quartile']
    features=rfmSeg[['R_Quartile','F_Quartile','M_Quartile']].values


    
    matrix = features

    km = KMeans(n_clusters = 2, n_init=2)
    km.fit(matrix)
    labels = km.labels_

    rfmSeg['response'] = labels
    print(rfmSeg.head())
    rfmSeg.shape


    
    Sim=pd.value_counts(rfmSeg['response']).plot.bar(title="Simulated Response",).get_figure()
    Sim
    Sim.savefig('C:/Users/12014/Documents/Python Scripts/PYTHON/Internship/static/Sim')


    
    rfmSeg.info()


    
    #customers with scores 3,4,5 are most likely to respond to the promotions.
    fig3=rfmSeg.groupby('Total Score').agg('response').mean().plot(x="Total Score",y="Proportion of Responders",kind='bar',
                                                                  colormap='Reds_r',title="Total_score vs Avg_Response").get_figure()
    #ax.set_xlabel("Total Score")
    #ax.set_ylabel("Proportion of Responders")
    
    fig3.savefig('C:/Users/12014/Documents/Python Scripts/PYTHON/Internship/static/ax')

    
    # Define rfm_level function
    def rfm_level(df):
        if df['Total Score'] >= 9:
            return 'Require Activation'
        elif ((df['Total Score'] >= 8) and (df['Total Score'] < 9)):
            return 'Needs Attention'
        elif ((df['Total Score'] >= 7) and (df['Total Score'] < 8)):
            return 'Promising'
        elif ((df['Total Score'] >= 6) and (df['Total Score'] < 7)):
            return 'Potential'
        elif ((df['Total Score'] >= 5) and (df['Total Score'] < 6)):
            return 'Loyal'
        elif ((df['Total Score'] >= 4) and (df['Total Score'] < 5)):
            return 'Champions'
        else:
            return 'Can\'t Loose Them'
    # Create a new variable RFM_Level
    rfmSeg['RFM_Level'] = rfmSeg.apply(rfm_level, axis=1)
    # Print the header with top 5 rows to the console
    rfmSeg.head()



    # Calculate average values for each RFM_Level, and return a size of each segment 
    rfm_level_agg = rfmSeg.groupby('RFM_Level').agg({
        'recency': 'mean',
        'frequency': 'mean',
        'monetary_value': ['mean', 'count']
    }).round(1)
    # Print the aggregated dataset
    print(rfm_level_agg)
    rfm_table= rfm_level_agg.to_html(header= 'true')

    
    rfm_level_agg.columns = rfm_level_agg.columns.droplevel()
    rfm_level_agg.columns = ['RecencyMean','FrequencyMean','MonetaryMean', 'Count']
    #Create our plot and resize it.
    fig = plt.gcf()
    ax = fig.add_subplot()
    fig.set_size_inches(16, 9)
    squarify.plot(sizes=rfm_level_agg['Count'], 
                label=['Can\'t Loose Them',
                        'Champions',
                        'Loyal',
                        'Needs Attention',
                        'Potential', 
                        'Promising', 
                        'Require Activation'], alpha=.6 )
    plt.title("RFM Segments",fontsize=18,fontweight="bold")
    plt.axis('off')
    #plt.show()
    plt.savefig('C:/Users/12014/Documents/Python Scripts/PYTHON/Internship/static/square.png')
    plt.show()
    
    return rfm_table


